# 威風避け

## 威風避けとは？

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">この避け方まじ良かった<br>ちょっと横ずれて押すだけ <a href="https://t.co/2Bm16glvFW">pic.twitter.com/2Bm16glvFW</a></p>&mdash; シンディ (@dqx_cyndi) <a href="https://twitter.com/dqx_cyndi/status/1122621107226304512?ref_src=twsrc%5Etfw">April 28, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

1. 横にずれる
2. 押す

と動くことにより、翠将の威風の範囲(前方扇形)から抜ける手法。

ジェルザークの強さに関係なく使用できる。だが、威風自体の発生が早く、さらに1→3でより早くなるため実際にはそれなりの反応速度が必要とされる。

亜種として敵の横へジャンプしてそのまま戻る方法も発見されている。

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">これでも威風と波動避けられますよ<a href="https://twitter.com/dq10_sofia?ref_src=twsrc%5Etfw">@dq10_sofia</a> <a href="https://t.co/Zpcc1paju3">pic.twitter.com/Zpcc1paju3</a></p>&mdash; オズワルドェェェァァ./Ninja250r (@OzWD_) <a href="https://twitter.com/OzWD_/status/1125414882990366720?ref_src=twsrc%5Etfw">May 6, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
