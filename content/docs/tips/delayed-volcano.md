# 遅延ボルケーノ

## 遅延ボルケーノとは？

リミットボルケーノのタイマーは発動時からだが、ダメージカウントは技の出る前、行動確定直後から発生している。ボルケーノも徴兵の大号令と同じく最寄りのプレイヤーをタゲにとって行動するためこれを引っ張ることによりボルケーノの発動を遅延させダメージを稼ぐことができる。

大号令飛ばしとは違い発生を遅延させることが目的なので攻撃役が壁に入るべきではないし、エンドまで引っ張るべきでもない。

タゲ補足から発動までわずかな時間が必要らしく、壁なしで逃げるとガクガクしながらプレイヤーについてきて意外と捕まらないので魔戦がひとりで逃げるぐらいがちょうどよいかと思われる。
