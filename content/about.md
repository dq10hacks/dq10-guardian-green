---
title: このサイトについて
type: docs
---

# このサイトについて

このサイトは [にゃ] が運営しております。

- twitter: [@nya_dq10](https://twitter.com/nya_dq10)

質問やご意見に関しては[Twitter](https://twitter.com/nya_dq10)か[Issue](https://gitlab.com/dq10hacks/dq10-guardian-green/issues)へお願いいたします。

[Merge Requests](https://gitlab.com/dq10hacks/dq10-guardian-green/merge_requests)も受け付けておりますので気軽に(?)ご寄稿ください。

----

「ドラゴンクエストX」におけるゲーム内容、画像、映像、キャラクター、音楽など、すべての表現物は、スクエア・エニックスを含む共同著作者もしくは、スクエア・エニックスが「ドラゴンクエストX」での利用に必要な権利の許諾を受けている権利者による著作物です。

（C）2017 ARMOR PROJECT/BIRD STUDIO/SQUARE ENIX All Rights Reserved.（C）SUGIYAMA KOBO（P）SUGIYAMA KOBO
